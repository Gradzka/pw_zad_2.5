// OpenMP_X_in_A.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stdlib.h"
#include "iostream"
#include "time.h"
#include "omp.h"


// *** MATRIX ***
int **create_matrix(int rows, int columns)
{
	int **matrix= new int*[rows];
	for (int i = 0; i < rows; i++)
		matrix[i] = new int[columns];
	return matrix;
}
void random_matrix(int **&matrix,int rows, int columns, int range)
{
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < columns; j++)
			matrix[i][j] = rand() % range;
}
void destroy_matrix(int **&matrix, int rows, int columns)
{
	for(int i = 0; i < rows; ++i)
		delete[] matrix[i];

	delete[] matrix;
}
void show_matrix(int **matrix, int rows, int columns)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			printf("%d\t",matrix[i][j]);
		}
		printf("\n");
	}
}
bool x_true_or_false_matrix(double &start,int x,int **matrix, int rows, int columns)
{
	bool result = false;
	int i, j;
	start = omp_get_wtime();
	//zrownoleglona petla for, zmienne prywatne to liczniki petli
	#pragma omp parallel for shared(matrix,rows,columns,result) private(i,j)
	for (i=0; i < rows; i++)
	{
		for (j=0; j < columns; j++)
		{
			if (result == true) break;
			else if (matrix[i][j] == x) { result = true; break; }
		}
	}
	return result;
}
int _tmain(int argc, _TCHAR* argv[])
{
	int **matrix;
	int rows=10, columns=10, range=2000,x;
	matrix=create_matrix(rows,columns);
	random_matrix(matrix, rows, columns, range);
	show_matrix(matrix,rows,columns);
	std::cout << "Podaj wartosc x: "<< std::endl;
	std::cin >>x;
	double start, end, result;

	if (x_true_or_false_matrix(start,x, matrix, rows, columns) == true)
	{
		end = omp_get_wtime();
		std::cout <<std::endl<< "Znalazlem!" << std::endl;
	}
	else { end = omp_get_wtime(); std::cout << "Nie znalazlem!" << std::endl; }
	result = end - start;
	std::cout <<std::endl<<"Czas wykonania w sekundach " << result << std::endl;
	destroy_matrix(matrix, rows, columns);
	

	return 0;
}

